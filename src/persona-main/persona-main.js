import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado';
import '../persona-form/persona-form';
import '../persona-main-dm/persona-main-dm';

class PersonaMain extends LitElement{

    static get properties() {
        return {
            people: {type: Array},
            showPersonForm: {type:Boolean},
            maxYearsInCompanyFilter: {type: Number},
        };
    }

    constructor(){
        super();

        this.people = [];

        this.showPersonForm = false;
        this.maxYearsInCompanyFilter = 3;
    }


    render() {
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        <h2 class="text-center">Personas</h2>
        <div class="row" id="peopleList">
            <div class="row row-cols-1 row-cols-sm-4">
            ${this.people.filter(person => person.yearsInCompany <= this.maxYearsInCompanyFilter
                ).map(
                    person => html`<persona-ficha-listado fname="${person.name}" yearsInCompany="${person.yearsInCompany}" profile="${person.profile}" .photo="${person.photo}" @delete-person="${this.deletePerson}" @info-person="${this.infoPerson}"></persona-ficha-listado>`
                )}
            </div>
        </div>
        <div class="row">
            <persona-form id="personForm" class="d-none rounded border-primary" @persona-form-close="${this.personFormClose}" @persona-form-store="${this.personFormStore}"></persona-form>
        </div>
        <persona-main-dm @people-data-updated="${this.peopleDataUpdated}"></persona-main-dm>
        `;
    }

    updated(changedProperties) {
        console.log("updated");

        if(changedProperties.has("showPersonForm")){
            console.log("ha cambiado el valor de la propiedad ShowPersonForm en persona-main");

            if(this.showPersonForm === true){
                this.showPersonFormData();
            }else{
                this.showPersonList();
            }
        }

        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-main");

            this.dispatchEvent(new CustomEvent("updated-people", {
                detail: {
                    people: this.people,
                }
            }))
        }

        if(changedProperties.has("maxYearsInCompanyFilter")){
            console.log("Ha cambiado el valor de la propiedad maxYearsInCompanyFilter en persona-main");
            console.log("Se van a mostrar las personas cuya antiguedad máxima sea " + this.maxYearsInCompanyFilter + " años");
        }
    }

    peopleDataUpdated(event){
        this.people = event.detail.people
    }

    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando el listado de personas");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    }

    showPersonFormData(){
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    personFormClose(){
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");

        this.showPersonForm = false;
    }

    personFormStore(e){
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");

        console.log("Name de la persona es " + e.detail.person.name);
        console.log("Profile de la persona es " + e.detail.person.profile);
        console.log("YearsInCompany de la persona es " + e.detail.person.yearsInCompany);

        if(e.detail.editingPerson === true){
            console.log("Se va a actualizar la persona de nombre " + e.detail.person.name);

            this.people = this.people.map(person => person.name === e.detail.person.name ? person = e.detail.person : person);
        }else{
            console.log("Se va a guardar una persona nueva");
            this.people = [...this.people, e.detail.person];
        }


        console.log("Fin proceso guardado");

        this.showPersonForm = false;
    }

    deletePerson(event){
        this.people = this.people.filter( person => person.name != event.detail.name);
    }

    infoPerson(event){
        console.log("Infoperson persona-main");
        console.log("Se ha pedido informacion de la persona " + event.detail.name);

        let choosenPerson = this.people.filter(person => person.name === event.detail.name);

        let person ={}
        person.name = choosenPerson[0].name;
        person.profile = choosenPerson[0].profile;
        person.yearsInCompany = choosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("personForm").person = person;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    }

}

customElements.define('persona-main', PersonaMain)