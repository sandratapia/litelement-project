import { LitElement, html } from 'lit-element';
import '../persona-header/persona-header';
import '../persona-main/persona-main';
import '../persona-sidebar/persona-sidebar';
import '../persona-footer/persona-footer';
import '../persona-stats/persona-stats';

class PersonaApp extends LitElement{

    static get properties() {
        return {
            people: {type: Array},
            maxYearsInCompanyFilter: {type: Number},
        };
    }

    constructor(){
        super();
    }


    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="row"> 
                <persona-sidebar class="col-2" @new-person="${this.newPerson}" @updated-max-years-filter="${this.newMaxYearsInCompanyFilter}"></persona-sidebar>
                <persona-main class="col-10" @updated-people="${this.updatedPeople}"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>
        `;
    }

    newPerson(event) {
        console.log("newPerson en persona-app");

        this.shadowRoot.querySelector("persona-main").showPersonForm = true;
    }

    newMaxYearsInCompanyFilter(event){
        console.log("newMaxYearsInCompanyFilter");
        console.log("Nuevo filtro es " + event.detail.maxYearsInCompanyFilter);

        this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = event.detail.maxYearsInCompanyFilter;
    }

    updated(changedProperties){
        console.log("updated en persona-app");

        if(changedProperties.has("people")){
            console.log("Ha cambiado la propiedad people en persona-app");
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    updatedPeople(event){
        console.log("updatedPeople en persona-app");

        this.people = event.detail.people;
    }

    updatedPeopleStats(event){
        console.log("updadtedPeopleStats");
        console.log(event.detail);

        this.shadowRoot.querySelector("persona-sidebar").peopleStats = event.detail.peopleStats;
        this.shadowRoot.querySelector("persona-main").maxYearsInCompanyFilter = event.detail.peopleStats.maxYearsInCompany;
    }

}

customElements.define('persona-app', PersonaApp)