import { LitElement, html } from 'lit-element';

class PersonaMainDM extends LitElement{

    static get properties() {
        return {
            people: {type: Array},

        };
    }

    constructor(){
        super();

        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque laoreet vel metus vitae scelerisque.",
                photo: {
                    src: "./img/persona.png",
                    alt:"Ellen Ripley"
                }
            },
            {
                name: "Bruce Banner",
                yearsInCompany: 2,
                profile: "Proin iaculis bibendum facilisis. Cras et nisl nec lacus laoreet faucibus. Nulla facilisi. Curabitur vestibulum justo sed erat hendrerit, at ullamcorper eros semper.",
                photo: {
                    src: "./img/persona.png",
                    alt:"Bruce Banner"
                }
            },
            {
                name: "Éowyn",
                yearsInCompany: 5,
                profile: "Sed vitae molestie dui. Aenean consequat at sem at elementum. In hac habitasse platea dictumst.",
                photo: {
                    src: "./img/persona.png",
                    alt:"Éowyn"
                }
            },
            {
                name: "Tyrion Lannister",
                yearsInCompany: 1,
                profile: "Nunc non magna vehicula, finibus ipsum vel, gravida tortor. Nulla dictum nunc vel magna varius vehicula. Ut egestas ultrices sem, ac congue tortor hendrerit vel.",
                photo: {
                    src: "./img/persona.png",
                    alt:"Tyrion Lannister"
                }
            },
            {
                name: "Turanga Leela",
                yearsInCompany: 9,
                profile: "Aenean faucibus, augue in commodo molestie, justo neque porta diam, quis efficitur ipsum mi eget justo. Fusce sed sagittis sem, in consectetur libero.",
                photo: {
                    src: "./img/persona.png",
                    alt:"Turanga Leela"
                }
            },
        ];
    }

    updated(changedProperties){
        console.log("updated");

        if(changedProperties.has("people")){
            console.log("Ha cambiado la propiedad people en persona dm");

            this.dispatchEvent(new CustomEvent("people-data-updated", {
                detail:{
                    people: this.people
                }
            }))
        }
    }

}

customElements.define('persona-main-dm', PersonaMainDM)