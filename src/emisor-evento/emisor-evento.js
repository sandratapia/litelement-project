import { LitElement, html } from 'lit-element';


class EmisorEvento extends LitElement{

    static get properties(){
        return{

        };
    }

    constructor(){
        super();
    }

    render() {
        return html`
            <div>
                <h3>Emisor Evento</h3>
                <button @click="${this.sendEvent}">No pulsar</button>
            </div>
        `;
    }

    sendEvent(event){
        console.log('sendEvent');
        console.log(event);
        
        this.dispatchEvent(new CustomEvent(
            "test-event",
            {
                "detail": {
                    "course": "techU",
                    "year": 2021
                }
            }
        ));
    }

}

customElements.define('emisor-evento', EmisorEvento)