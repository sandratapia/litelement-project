import { LitElement, html } from 'lit-element';
import '../emisor-evento/emisor-evento';
import '../receptor-evento/receptor-evento';

class GestorEvento extends LitElement{

    static get properties(){
        return{

        };
    }

    constructor(){
        super();
    }

    render() {
        return html`
            <div>
                <h1>Gestor Evento</h1>
                <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
                <receptor-evento id="receiver"></receptor-evento>
            </div>
        `;
    }

    processEvent(event){
        console.log("processEvent");
        console.log(event);

        this.shadowRoot.getElementById("receiver").course = event.detail.course;
        this.shadowRoot.getElementById("receiver").year = event.detail.year;

    }

}

customElements.define('gestor-evento', GestorEvento)