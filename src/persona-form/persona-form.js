import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement{

    static get properties() {
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        };
    }

    constructor(){
        super();

        this.resetFormData();
    }


    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">
                        <label for="name">Nombre completo</label>
                        <input type="text" name="name" class="form-control" placeholder="Nombre completo" @input="${this.updateName}" .value="${this.person.name}" ?disabled="${this.editingPerson}"/>
                    </div>
                    <div class="form-group">
                        <label for="profile">Perfil</label>
                        <textarea rows="5" class="form-control" placeholder="Perfil" @input="${this.updateProfile}" .value="${this.person.profile}"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="yearsInCompany">Años en la empresa</label>
                        <input type="text" name="name" class="form-control" placeholder="Años en la empresa" @input="${this.updateYearsInCompany}" .value="${this.person.yearsInCompany}"/>
                    </div>
                    <button class="btn btn-primary" @click="${this.goBack}"><strong>Atrás</strong></button>
                    <button class="btn btn-success" @click="${this.storePerson}"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    updateName(event){
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor " + event.target.value);

        this.person.name = event.target.value;
    }

    updateProfile(event){
        console.log("updateProfile");
        console.log("Actualizando la propiedad profile con el valor " + event.target.value);

        this.person.profile = event.target.value;
    }

    updateYearsInCompany(event){
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad yearsInCompany con el valor " + event.target.value);

        this.person.yearsInCompany = event.target.value;
    }

    goBack(event){
        console.log("goBack");
        event.preventDefault();

        this.dispatchEvent(new CustomEvent("persona-form-close", ))

        this.resetFormData();
    }

    resetFormData(){
        console.log("resetFormData")

        this.person = {};
        this.person.name = "";
        this.person.profile= "";
        this.person.yearsInCompany = "";

        this.editingPerson = false;
    }

    storePerson(event){
        console.log("storePerson");
        console.log("Se va a guardar una persona");
        event.preventDefault();

        console.log("Actualizando la propiedad name con el valor " + this.person.name);
        console.log("Actualizando la propiedad profile con el valor " + this.person.profile);
        console.log("Actualizando la propiedad yearsInCompany con el valor " + this.person.yearsInCompany);

        this.person.photo = {
            "src": "../../img/persona.png",
            "alt": "persona"
        }

        this.dispatchEvent(new CustomEvent("persona-form-store", {
            detail: {
                person: {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo,
                },
                editingPerson: this.editingPerson,
            }
        }))
    }

}

customElements.define('persona-form', PersonaForm)