import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement{

    static get properties() {
        return {
            name:{type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},

        };
    }

    constructor(){
        super();
        this.name = "Prueba nombre";
        this.yearsInCompany = 12;

        this.updatePersonInfo();
    }

    updated(changedProperties){
        console.log("updated");
        //SE LLAMA DESPUES DE HABER CAMBIADO EL VALOR DE UNA PROPIEDAD
        changedProperties.forEach((oldValue, propName) => {
            console.log("Propiedad " + propName + " cambia valor, anterior era " + oldValue)
        });

        if(changedProperties.has("name")){
            console.log("Propiedad name ha cambiado de valor, anterior era " + changedProperties.get("name") + " nuevo es " + this.name);
        }
        if(changedProperties.has("yearsInCompany")){
            console.log("Propiedar yearsInCompany cambiada valor anterior era " + changedProperties.get("yearsInCompany") + " nuevo es " + this.personInfo);
            this.updatePersonInfo();
        }
    }

    render() {
        return html`
        <div>
            <label>Nombre completo</label>
            <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"/>
            <br />
            <label>Años en la empresa</label>
            <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"/>
            <br />
            <input type="text" value="${this.personInfo}" @change="${this.updatePersonInfo}" disabled/>
            <br />
        </div>
        `;
    }

    updateName(event){
        console.log("updateName");
        this.name = event.target.value;
    }

    updateYearsInCompany(event){
        console.log("updateYearsInCompany");
        this.yearsInCompany = event.target.value;
    }

    updatePersonInfo(){
        console.log("updatePersonInfo");

        if(this.yearsInCompany >= 7){
            this.personInfo = "lead";
        }else if(this.yearsInCompany >= 5){
            this.personInfo ="senior";
        }else if (this.yearsInCompany >=3){
            this.personInfo ="team";
        }else {
            this.personInfo ="junior";
        }
    }
}

customElements.define('ficha-persona', FichaPersona)